import React, {Component} from "react";
import PostforHome from './PostforHome';
import axios from '../axios-posts';

class Home extends Component {
  state = {
    posts: []
  };

  componentDidMount() {
    axios.get('/posts.json').then((response) => {
      let data = Object.values(response.data)
      const keys = Object.keys(response.data);
      for(let i=0;i<keys.length; i++)
      {
        data[i].id = keys[i];
      }
      this.setState({ posts : data });
    }).catch(error => {
      console.log(error);
    });
  }

  render() {
    let form = null;
    if(this.state.posts.length > 0)
      form = this.state.posts.map((post) => {
        return <PostforHome 
          date = {post.creationDate}
          title={post.title}
          key={post.id} 
          id={post.id}>
          </PostforHome>
      });
    return (
      <div>        
        {
          form
        }
      </div>
    );  
  }
}

export default Home;
