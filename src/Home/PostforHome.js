import React from 'react';
import { Link } from 'react-router-dom';

//import './task.css';

const PostforHome = (props) => {
    return (
        <div className='container'>
            <ul>Created on: {props.date}</ul>
                         
            <h2><Link to={`/posts/${props.id}`}>{props.title}</Link></h2> 
            <p><Link to={`/posts/${props.id}`}>Read more>>>></Link></p> 
            
        </div>

    );
}

export default PostforHome;