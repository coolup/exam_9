import React from 'react';
import AddPostForm from '../Post/AddPostForm';
import Post from '../Post/Post';
import '../App.css';
import axios from '../axios-posts';

class PostBuilder extends React.Component {
    state = {
      title: '',
      currentPost: '',
      currentId: 0,
      posts: [],
    };

    AddPostTheme = (event) => {
      const title = event.target.value;
      this.setState({ title });
    }
  
    AddPostDesc = (event) => {
      const currentPost = event.target.value;
      this.setState({ currentPost });
    }
  
    AddPost = () => {
      const descriptionValue = this.state.currentPost;
      const titleValue = this.state.title;    
  
      let postId = ++this.state.currentId;
      const date = this.getDateToString();
  
      const newPost = {
        creationDate: date,
        description: descriptionValue,
        title: titleValue,
        id: postId
      };
      const allPosts = this.state.posts;
      allPosts.push(newPost);
        
      const json = {
          currentId: postId,
          posts: allPosts
      }
      axios.put(".json", json);
      this.setState({ allPosts });
    };

    getDateToString() {
      const date = new Date();
      var stringDate =
        date.getDate() +
        "." +
        date.getMonth() +
        "." +
        date.getFullYear() +
        " " +
        date.getHours() +
        ":" +
        date.getMinutes();
      return stringDate;
    }

    removePost = (id) => {      
      axios.delete('/posts/' + id + '.json').then(response => {
        const index = this.state.posts.findIndex(t => t.id === id);
        const posts = [...this.state.posts];
        posts.splice(index, 1);
        this.setState({ posts });
      });
    }

    editPost = (id) => {      
      const editedPost = {
        title: this.state.title,
        description: this.state.currentPost
      }
      axios.put('/posts/' + id + '.json', editedPost).then(response => {
        const index = this.state.posts.findIndex(t => t.id === id);
        const posts = [...this.state.posts];
        posts[index].description = editedPost.description;
        this.setState({ posts });
      });
    }
  
    componentDidMount() {
      axios.get('/posts.json').then((response) => {
        let data = Object.values(response.data)
        const keys = Object.keys(response.data);
        for(let i=0;i<keys.length; i++)
        {
          data[i].id = keys[i];
        }
        this.setState({ posts : data });
      }).catch(error => {
        console.log(error);
      });
    }
  
    render() {
      let form = null;
      if(this.state.posts.length > 0)
        form = this.state.posts.map((post) => {
          return <Post 
            title={post.title}
            desc={post.description} 
            key={post.id} 
            id={post.id} 
            date = {post.creationDate}
            remove={() => this.removePost(post.id)}
            edit={() => this.editPost(post.id)}>
            </Post>
        });
      return (
        <div>
          <AddPostForm  newtheme={event => this.AddPostTheme(event)}
                        newform={event => this.AddPostDesc(event)} 
                        add={() => this.AddPost(this.state.posts.length)} />
          {
            form
          }
        </div>
      );
    }
  }

  export default PostBuilder;