import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://my-project-8c463.firebaseio.com/' // Your URL here!
});

export default instance;
