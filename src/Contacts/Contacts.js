import React from "react";

function Contacts() {
  return (
    <div>
      <h2>Contacts INFO</h2>
      <p>Salisbury House, Station Road</p>
      <p>CB12LA Cambridge, United Kingdom</p>
      <p>+44 20 3984 8555</p> 

      <h6>CONTACT US</h6>
      <p>dreamteam@pussycat.io</p>     
    </div>
  );
}

export default Contacts;
