import React from 'react';
import './task.css';

const AddPostFrom = (props) => {
    return (
        <div className="taskform">
            <input type="text" placeholder="Theme" onChange={props.newtheme}/>
            <input type="text" placeholder="Add post" onChange={props.newform}/>
            <input type ="button" value="Add" onClick={props.add}/>
        </div>
    );
}
export default AddPostFrom;