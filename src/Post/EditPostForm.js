import React from 'react';
import './task.css';

const EditPostFrom = (props) => {
    console.log("i am in edit task form " + props.name);
    return (
        <div className="taskform">
            <input type="text" placeholder="Edit Theme" value={props.title} onChange={props.newtheme}/>
            <input type="text" placeholder="Edit post" value={props.description} onChange={props.newform}/>
            <input type ="button" value="Edit" onClick={props.edit}/>
        </div>
    );
}
export default EditPostFrom;