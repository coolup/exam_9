import React, { Component } from "react";
import axios from '../axios-posts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faTrashAlt, faEdit} from '@fortawesome/free-solid-svg-icons';
import { BrowserRouter, Route, Switch, NavLink, Link } from 'react-router-dom';
import EditPostFrom from './EditPostForm';

class PostDetail extends Component {
  state = {
    post: []
  };

  componentDidMount() {
    let id = this.props.match.params.id
    axios.get(`/posts/${id}.json`).then((response) => {
      this.setState({ post : response.data });
      }).then(()=>{
          console.log(this.state.post);
      }).catch(error => {
          console.log(error);
      });
  }

  render() {
    return (
      <div className="container">
        <div className="Post justify-content-center align-items-center">
          <div>created at: {this.state.post.creationDate}</div>
          <div>title: {this.state.post.title}</div>
          <div>description: {this.state.post.description}</div>
        </div>
        <FontAwesomeIcon icon={faEdit} className="icon" onClick={<Route exact path='/posts/:id' component={EditPostFrom} />}/>
        <FontAwesomeIcon icon={faTrashAlt} className="icon" onClick={this.props.remove}/>
      </div>
    );
  }
}

export default PostDetail;