import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faTrashAlt, faEdit} from '@fortawesome/free-solid-svg-icons';
import { Link, Route } from 'react-router-dom';

import './task.css';

const Post = (props) => {
    return (
        <div className="task">
            <Link to={`/posts/${props.id}`}>{props.title}</Link>
            <Link to={`/posts/${props.id}`}>{props.desc}</Link>
            <Link to={`/posts/${props.id}`}>{props.date}</Link>
            <FontAwesomeIcon icon={faEdit} className="icon" onClick={props.edit}/>
            <FontAwesomeIcon icon={faTrashAlt} className="icon" onClick={props.remove}/>
        </div>

    );
}

export default Post;