import React from 'react';
import PostBuilder from './containers/PostBuilder';
import { BrowserRouter, Route, Switch, NavLink, Link } from 'react-router-dom';
import EditPostFrom from './Post/EditPostForm';
import AddPostFrom from './Post/AddPostForm';
import About from "./About/About";
import Contacts from "./Contacts/Contacts";
import Home from "./Home/Home";
import PostDetail from "./Post/PostDetail"



function App() {
  return (
    <div className="App container">
      <BrowserRouter>
        <Nav />
        <Switch>
          <Route path='/' exact component={Home}/>
          <Route path='/posts' exact component={Home}/>
          <Route path='/posts/add' exact component={PostBuilder}/>
          <Route exact path='/posts/:id' component={PostDetail} />
          <Route path='/about' component={About}/>
          <Route path='/contacts' component={Contacts}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

function Nav() {
  return (
    <nav className="nav-class">      
      <Link to='/' className='brand-logo'>MY BLOG</Link>
      <NavLink to="/posts">
        Home
      </NavLink>
      <NavLink to="/posts/add">
        Add
      </NavLink>
      <NavLink to="/about">
        About
      </NavLink>
      <NavLink to="/contacts">
        Contacts
      </NavLink>
    </nav>
  );
}

export default App;
