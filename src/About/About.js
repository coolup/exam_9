import React from "react";

function About() {
  return (
    <div>
      <h2>About US</h2>
      <ul  className='text-justify'>
        <li>
          Best Practices and Professional Development Approaches
        </li>
        <img data-v-4c783810="" src="https://maddevs.io/_nuxt/img/5b33a3a.svg" ></img>
        <li>
          We design a technical team for you, manage it and closely communicate with you and inside the team.
        </li>
        <img data-v-4c783810="" src="https://maddevs.io/_nuxt/img/7002693.svg" ></img>
        <li>
          We solve the full range of IT services with different levels of customer involvement          
        </li>
        <img data-v-4c783810="" src="https://maddevs.io/_nuxt/img/79aedcd.svg" ></img>
        <li>Quick Project Start          
        </li>
        <img data-v-4c783810="" src="https://maddevs.io/_nuxt/img/2df694e.svg" ></img>
        <li>Technical Expertise          
        </li>
        <img data-v-4c783810="" src="https://maddevs.io/_nuxt/img/c3f727d.svg" ></img>
        <li>
          Besides their main job, our teams contribute to open source
        </li>
      </ul>
    </div>
  );
}

export default About;
